package com.example.demo.lab34;

public interface NotesService {
    void add(Note note);

    float averageOf(String name);

    void clear();
}
