package com.example.demo.lab56;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Component;

@Component
public class ReverseUppercase implements ReverseLetters {

    @Override
    public String apply(final String sentence) {
        Preconditions.checkArgument(sentence != null);
        if ("".equals(sentence)) {
            return "";
        }
        boolean endWhiteSpace = sentence.charAt(sentence.length() - 1) == ' ';
        String[] words = sentence.split(" ");
        StringBuilder reversedString = new StringBuilder();
        for (int i = 0, wordsLength = words.length; i < wordsLength; i++) {
            String word = words[i];
            StringBuilder reverseWord = new StringBuilder();
            for (int j = word.length() - 1; j >= 0; j--) {
                reverseWord.append(word.charAt(j));
            }
            reversedString.append(reverseWord);
            if (i != wordsLength - 1 || endWhiteSpace) {
                reversedString.append(" ");
            }
        }
        return reversedString.toString().toUpperCase();
    }
}
