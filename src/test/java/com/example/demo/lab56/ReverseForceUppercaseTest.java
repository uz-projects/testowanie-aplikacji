package com.example.demo.lab56;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ReverseForceUppercaseTest {

    @Autowired
    private ReverseForceUppercase reversePhrase;

    @Test
    public void apply_null() {
        assertThrows(IllegalArgumentException.class, () -> reversePhrase.apply(null));
    }

    @Test
    public void apply_empty() {
        assertEquals("", reversePhrase.apply(""));
    }

    @Test
    public void apply_plainText() {
        assertEquals("ZAMIENIONA KOLEJNOŚĆ LITER", reversePhrase.apply("anoineimaZ ćśonjelok retil"));
    }

    @Test
    public void apply_textWithSpaces() {
        assertEquals(" ZAMIENIONA KOLEJNOŚĆ LITER ", reversePhrase.apply(" anoineimaZ ćśonjelok retil "));
    }

    @Test
    public void apply_textWithNumbers() {
        assertThrows(IllegalArgumentException.class, () -> reversePhrase.apply("test 123 test"));
    }
}
