package com.example.demo.lab56;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReverseWordsTest {
    private static ReverseLetters reverseWords;

    @BeforeAll
    static void before() {
        final ReverseLetters reversePhrase = mock(ReverseLetters.class);
        when(reversePhrase.apply("Zamieniona")).thenReturn("anoineimaZ");
        when(reversePhrase.apply("kolejność")).thenReturn("ćśonjelok");
        when(reversePhrase.apply("liter")).thenReturn("retil");
        reverseWords = ReverseWords.with(reversePhrase);
    }

    @Test
    public void create_withNullReversePhrase() {
        assertThrows(IllegalArgumentException.class, () -> ReverseWords.with(null));
    }

    @Test
    public void apply_null() {
        assertThrows(IllegalArgumentException.class, () -> reverseWords.apply(null));
    }

    @Test
    public void apply_empty() {
        assertEquals("", reverseWords.apply(""));
    }

    @Test
    public void apply_plainText() {
        assertEquals("anoineimaZ ćśonjelok retil", reverseWords.apply("Zamieniona kolejność liter"));
    }

    @Test
    public void apply_textWithSpaces() {
        assertEquals(" anoineimaZ ćśonjelok retil ", reverseWords.apply(" Zamieniona kolejność liter "));
    }
}
