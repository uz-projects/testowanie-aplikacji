package com.example.demo.lab56;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

public class ReverseWords implements ReverseLetters {
    private final ReverseLetters reversePhrase;

    public static ReverseWords with(final ReverseLetters reversePhrase) {
        return new ReverseWords(reversePhrase);
    }

    @Override
    public String apply(final String sentence) {
        Preconditions.checkArgument(sentence != null);
        if ("".equals(sentence)) {
            return "";
        }
        boolean startWhiteSpace = false;
        boolean endWhiteSpace = false;
        if (sentence.charAt(0) == ' ') {
            startWhiteSpace = true;
        }
        if (sentence.charAt(sentence.length() - 1) == ' ') {
            endWhiteSpace = true;
        }
        final Iterable<String> words = Splitter.on(' ').split(sentence.trim());
        String startWs = startWhiteSpace ? " " : "";
        String endWs = endWhiteSpace ? " " : "";
        return startWs + Joiner.on(' ').join(Iterables.transform(words, reversePhrase)) + endWs;
    }

    private ReverseWords(final ReverseLetters reversePhrase) {
        Preconditions.checkArgument(reversePhrase != null);
        this.reversePhrase = reversePhrase;
    }
}
