package com.example.demo.lab56;

import com.google.common.base.Function;

public interface ReverseLetters extends Function<String, String> {
}
