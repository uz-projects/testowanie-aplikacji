package com.example.demo.lab34;

import com.google.common.base.Preconditions;

import java.util.Collection;

public class NotesServiceImpl implements NotesService {
    public static NotesService createWith(final NotesStorage storageService) {
        return new NotesServiceImpl(storageService);
    }

    @Override
    public void add(final Note note) {
        storageService.add(note);
    }

    @Override
    public float averageOf(final String name) {
        Preconditions.checkArgument(name != null, "Imię ucznia nie może być wartością null.");
        float sum = 0.0f;
        final Collection<Note> notes = storageService.getAllNotesOf(name);
        if (notes.size() == 0) {
            return 0.0f;
        }
        for (final Note note : notes) {
            sum += note.getNote();
        }
        return sum / notes.size();
    }

    @Override
    public void clear() {
        storageService.clear();
    }

    private NotesServiceImpl(final NotesStorage storageService) {
        this.storageService = storageService;
    }

    private final NotesStorage storageService;
}