package com.example.demo.lab12;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class Lab12Test {

    @Autowired
    private Lab12 lab12;

    @Test
    void Factorial_Should_Return6_When_InputIs3() {
        var result = lab12.factorial(3);

        assertEquals(6, result);
    }

    @Test
    void Factorial_Should_Return1_When_InputIs0() {
        var result = lab12.factorial(0);

        assertEquals(1, result);
    }

    @Test
    void IsPrime_Should_ReturnFalse_When_InputIs0() {
        var result = lab12.isPrime(0);

        assertFalse(result);
    }

    @Test
    void IsPrime_Should_ReturnFalse_When_InputIs1() {
        var result = lab12.isPrime(1);

        assertFalse(result);
    }

    @Test
    void IsPrime_Should_ReturnTrue_When_InputIs2() {
        var result = lab12.isPrime(2);

        assertTrue(result);
    }

    @Test
    void SortIntegerArray_Should_SortAsc_When_InputIsArrayAndAsc() {
        var testArray = new Integer[]{11, 2, 3, 8, 6, 4, 2, 8, 9, 4};
        var expectedArray = new Integer[]{2, 2, 3, 4, 4, 6, 8, 8, 9, 11};
        var sortedArray = lab12.sortIntegerArray(testArray, "asc");

        assertArrayEquals(expectedArray, sortedArray);
    }

    @Test
    void SortIntegerArray_Should_SortDesc_When_InputIsArrayAndDesc() {
        var testArray = new Integer[]{11, 2, 3, 8, 6, 4, 2, 8, 9, 4};
        var expectedArray = new Integer[]{11, 9, 8, 8, 6, 4, 4, 3, 2, 2};
        var sortedArray = lab12.sortIntegerArray(testArray, "desc");

        assertArrayEquals(expectedArray, sortedArray);
    }

    @Test
    void SortIntegerArray_Should_ThrowIllegalArgumentException_When_DirIsNotAscOrDesc() {
        assertThrows(IllegalArgumentException.class,
                     () -> lab12.sortIntegerArray(new Integer[]{}, "illegal argument"));
    }
}
