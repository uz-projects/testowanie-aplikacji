package com.example.demo.lab34;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class NoteTest {

    @Test
    void Should_ThrowIllegalArgumentException_When_NameIsNull() {
        assertThrows(IllegalArgumentException.class, () -> Note.of(null, 2.0f));
    }

    @Test
    void Should_ThrowIllegalArgumentException_When_NameIsEmpty() {
        assertThrows(IllegalArgumentException.class, () -> Note.of("", 2.0f));
    }

    @Test
    void Should_ThrowIllegalArgumentException_When_NoteIsLessThan2OrGreaterThan6() {
        assertThrows(IllegalArgumentException.class, () -> Note.of("Kacper", 1.0f));
        assertThrows(IllegalArgumentException.class, () -> Note.of("Kacper", 7.0f));
    }
}
