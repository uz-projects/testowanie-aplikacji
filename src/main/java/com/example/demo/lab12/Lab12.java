package com.example.demo.lab12;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;


@Component
public class Lab12 {

    public int factorial(int n) {
        return IntStream.rangeClosed(1, n)
                .reduce(1, (int x, int y) -> x * y);
    }

    public boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public Integer[] sortIntegerArray(Integer[] array, String dir) {
        if ("asc".equals(dir)) {
            Arrays.sort(array);
            return array;
        } else if ("desc".equals(dir)) {
            Arrays.sort(array, Collections.reverseOrder());
            return array;
        } else {
            throw new IllegalArgumentException();
        }
    }
}
