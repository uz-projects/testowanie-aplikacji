package com.example.demo.lab56;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ReversePhraseTest {

    @Autowired
    private ReversePhrase reversePhrase;

    @Test
    public void apply_null() {
        assertThrows(IllegalArgumentException.class, () -> reversePhrase.apply(null));
    }

    @Test
    public void apply_empty() {
        assertEquals("", reversePhrase.apply(""));
    }

    @Test
    public void apply_plainText() {
        assertEquals("anoineimaZ ćśonjelok retil", reversePhrase.apply("Zamieniona kolejność liter"));
    }

    @Test
    public void apply_textWithSpaces() {
        assertEquals(" anoineimaZ ćśonjelok retil ", reversePhrase.apply(" Zamieniona kolejność liter "));
    }
}
